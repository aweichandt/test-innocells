import React, {useMemo} from 'react';
import {WebView} from 'react-native-webview';

type Props = {id: string};

const BASE_URL = 'https://reddit.com';

const Screen: React.FC<Props> = ({id}) => {
  const uri = useMemo(() => `${BASE_URL}/${id}`, [id]);
  return <WebView source={{uri}} />;
};

export default Screen;
