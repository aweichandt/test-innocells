import {MapStateToProps, connect} from 'react-redux';
import {StateWithMain, chosenPostId} from '../../main/module';
import {Detail} from '../ui';

type Props = {};
type SProps = {id: string};

const mapStateToProps: MapStateToProps<
  SProps,
  Props,
  StateWithMain
> = state => ({
  id: chosenPostId(state),
});

export default connect(
  mapStateToProps,
  null,
)(Detail);
