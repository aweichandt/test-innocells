import React from 'react';
import {SafeAreaView} from 'react-native';
import {RedditPostDetail} from './containers';

const Screen: React.FC = () => (
  <SafeAreaView style={{flex: 1}}>
    <RedditPostDetail />
  </SafeAreaView>
);

export default Screen;
