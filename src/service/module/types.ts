export type Entity<T extends {}> = {
  id: string;
} & T;

export type EntityMap<T extends {}> = {[id: string]: T};

export type Post = {
  title: string;
  author: string;
  created_utc: number;
  score: number;
  num_comments: number;
  thumbnail: string;
  thumbnail_width: number;
  thumbnail_height: number;
};

export type RawData = {
  data: {
    children: {data: Entity<Post>}[];
  };
};

export type Sort = 'new' | 'hot' | 'top' | 'controversial';

export type State = {
  posts: EntityMap<Post>;
  refreshing: boolean;
};

export type StateWithService = {
  service: State;
};
