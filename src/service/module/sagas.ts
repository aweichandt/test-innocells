import {call, put, select, takeLatest} from 'redux-saga/effects';
import {Sort, RawData, EntityMap, Post} from './types';
import normaliseData from './normaliser';
import {storePosts, REFRESH_POSTS, RefreshPosts} from './actions';
import {posts} from './selectors';

export const BASE_URL = 'https://api.reddit.com';

export const fetchPosts = async (sort: Sort): Promise<RawData> => {
  const response = await fetch(`${BASE_URL}/r/pics/${sort}.json`, {
    method: 'GET',
    headers: {'Content-Type': 'Application/json'},
  });
  if (response.ok) {
    const result: RawData = await response.json();
    return result;
  }
  throw new Error('network error');
};

export function* onRefreshPosts(action?: RefreshPosts) {
  const sort = action ? action.payload : 'new';
  try {
    const data: RawData = yield call(fetchPosts, sort);
    const normalised: EntityMap<Post> = yield call(normaliseData, data);
    yield put(storePosts(normalised));
  } catch (error) {
    const currentData = yield select(posts);
    yield put(storePosts(currentData));
  }
}

export function* listen() {
  yield takeLatest(REFRESH_POSTS, onRefreshPosts);
}

export default [listen, onRefreshPosts];
