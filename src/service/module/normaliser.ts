import {RawData, EntityMap, Post} from './types';

const normaliseData = (data: RawData): EntityMap<Post> => {
  const {
    data: {children},
  } = data;
  return children.reduce<EntityMap<Post>>((acc, item) => {
    const {
      data: {id, ...post},
    } = item;
    acc[id] = post;
    return acc;
  }, {});
};

export default normaliseData;
