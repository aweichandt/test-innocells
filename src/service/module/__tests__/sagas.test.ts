import {call, put, takeLatest, select} from 'redux-saga/effects';
import {onRefreshPosts, fetchPosts, listen} from '../sagas';
import normaliseData from '../normaliser';
import {RawData, EntityMap, Post} from '../types';
import {storePosts, REFRESH_POSTS, refreshPosts} from '../actions';
import {posts} from '../selectors';

const post = {
  title: 'teest',
  created_utc: 1,
  score: 1,
  author: 'someone',
  num_comments: 1,
  thumbnail: 'url',
  thumbnail_width: 1,
  thumbnail_height: 1,
};
const rawData: RawData = {
  data: {children: [{data: {id: 'post1', ...post}}]},
};
const normalisedData: EntityMap<Post> = {
  post1: post,
};

describe('service sagas', () => {
  describe('on resfresh posts', () => {
    it('with sort order', () => {
      const action = refreshPosts('top');
      const gen = onRefreshPosts(action);
      expect(gen.next().value).toEqual(call(fetchPosts, 'top'));
      // @ts-ignore
      expect(gen.next(rawData).value).toEqual(call(normaliseData, rawData));
      // @ts-ignore
      expect(gen.next(normalisedData).value).toEqual(
        put(storePosts(normalisedData)),
      );
      expect(gen.next().done).toBeTruthy();
    });
    it('with success reesult', () => {
      const gen = onRefreshPosts();
      expect(gen.next().value).toEqual(call(fetchPosts, 'new'));
      // @ts-ignore
      expect(gen.next(rawData).value).toEqual(call(normaliseData, rawData));
      // @ts-ignore
      expect(gen.next(normalisedData).value).toEqual(
        put(storePosts(normalisedData)),
      );
      expect(gen.next().done).toBeTruthy();
    });
    it('with failed fetch', () => {
      const gen = onRefreshPosts();
      expect(gen.next().value).toEqual(call(fetchPosts, 'new'));
      expect(gen.throw(new Error('test')).value).toEqual(select(posts));
      // @ts-ignore
      expect(gen.next(normalisedData).value).toEqual(
        put(storePosts(normalisedData)),
      );
      expect(gen.next().done).toBeTruthy();
    });
  });
  it('listen', () => {
    const gen = listen();
    expect(gen.next().value).toEqual(takeLatest(REFRESH_POSTS, onRefreshPosts));
    expect(gen.next().done).toBeTruthy();
  });
});
