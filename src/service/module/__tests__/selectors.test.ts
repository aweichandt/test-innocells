import {posts, postIds, postWithId, refreshing} from '../selectors';

const post1 = {
  title: 'teest',
  created_utc: 1,
  score: 1,
  author: 'someone',
  num_comments: 1,
  thumbnail: 'url',
  thumbnail_width: 1,
  thumbnail_height: 1,
};

const state = {
  service: {
    posts: {
      post1,
    },
    refreshing: false,
  },
};

describe('service selectors', () => {
  it('get posts', () => {
    expect(posts(state)).toEqual({post1});
  });
  it('get post ids', () => {
    expect(postIds(state)).toEqual(['post1']);
  });
  describe('get post with id', () => {
    it('matching id', () => {
      expect(postWithId(state, {id: 'post1'})).toEqual(post1);
    });
    it('mismatching id', () => {
      expect(postWithId(state, {id: 'other'})).toBeUndefined();
    });
  });
  it('is refreshing', () => {
    expect(refreshing(state)).toEqual(false);
  });
});
