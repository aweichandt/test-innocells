import {refreshPosts, storePosts} from '../actions';

const mockData = {
  id: {
    title: 'teest',
    created_utc: 1,
    score: 1,
    author: 'someone',
    num_comments: 1,
    thumbnail: 'url',
    thumbnail_width: 1,
    thumbnail_height: 1,
  },
};

describe('service actions', () => {
  describe('refresh posts', () => {
    it('without order', () => {
      expect(refreshPosts()).toMatchSnapshot();
    });
    it('with order', () => {
      expect(refreshPosts('top')).toMatchSnapshot();
    });
  });
  it('store posts', () => {
    expect(storePosts(mockData)).toMatchSnapshot();
  });
});
