import reducer, {initialState} from '../reducer';
import {storePosts, refreshPosts} from '../actions';

const mockData = {
  id: {
    title: 'teest',
    created_utc: 1,
    score: 1,
    author: 'someone',
    num_comments: 2,
    thumbnail: 'url',
    thumbnail_width: 1,
    thumbnail_height: 1,
  },
};

describe('service reducer', () => {
  it('initialise with state', () => {
    expect(reducer(undefined, {type: '@@init'})).toEqual(initialState);
  });
  it('refresh posts data', () => {
    expect(reducer(initialState, refreshPosts())).toEqual({
      ...initialState,
      refreshing: true,
    });
  });
  it('stores posts data', () => {
    expect(reducer(initialState, storePosts(mockData))).toEqual({
      ...initialState,
      posts: mockData,
      refreshing: false,
    });
  });
});
