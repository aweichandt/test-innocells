import {Post, RawData} from '../types';
import normaliseData from '../normaliser';

const post1: Post = {
  title: 'teest',
  created_utc: 1,
  score: 1,
  author: 'someone',
  num_comments: 1,
  thumbnail: 'url',
  thumbnail_width: 1,
  thumbnail_height: 1,
};

const post2: Post = {
  title: 'other',
  created_utc: 2,
  score: 2,
  author: 'other',
  num_comments: 2,
  thumbnail: 'uotherrl',
  thumbnail_width: 2,
  thumbnail_height: 2,
};

const raw: RawData = {
  data: {
    children: [{data: {id: 'id1', ...post1}}, {data: {id: 'id2', ...post2}}],
  },
};

describe('normaliser test', () => {
  it('normalises data', () => {
    expect(normaliseData(raw)).toEqual({
      id1: post1,
      id2: post2,
    });
  });
});
