import {StorePosts, STORE_POSTS, RefreshPosts, REFRESH_POSTS} from './actions';
import {State} from './types';
import {createReducer} from '../../redux-helpers';

export const initialState: State = {
  posts: {},
  refreshing: false,
};

const onRefreshPosts = (state: State, _: RefreshPosts): State => ({
  ...state,
  refreshing: true,
});

const onStorePosts = (state: State, {payload}: StorePosts): State => ({
  ...state,
  posts: payload,
  refreshing: false,
});

const handlers = Object.freeze({
  [REFRESH_POSTS]: onRefreshPosts,
  [STORE_POSTS]: onStorePosts,
});

export default createReducer(handlers, initialState);
