import {StateWithService, State, EntityMap, Post} from './types';
import {Selector, createSelector, ParametricSelector} from 'reselect';

const stateSelector = ({service}: StateWithService): State => service;

const propsSelector = <T>(_: StateWithService, props: T): T => props;

export const posts: Selector<
  StateWithService,
  EntityMap<Post>
> = createSelector(
  stateSelector,
  ({posts}) => posts,
);

export const refreshing: Selector<StateWithService, boolean> = createSelector(
  stateSelector,
  ({refreshing}) => refreshing,
);

export const postIds: Selector<StateWithService, string[]> = createSelector(
  posts,
  posts => Object.keys(posts),
);

type EntityProps = {id: string};
export const postWithId: ParametricSelector<
  StateWithService,
  EntityProps,
  Post | void
> = createSelector(
  propsSelector,
  posts,
  ({id}: EntityProps, posts) => posts[id],
);
