import {PayloadAction} from '../../redux-helpers';
import {EntityMap, Post, Sort} from './types';

export const REFRESH_POSTS = '@service/REFRESH_POSTS';
export const STORE_POSTS = '@service/STORE_POSTS';

export type RefreshPosts = PayloadAction<typeof REFRESH_POSTS, Sort>;
export type StorePosts = PayloadAction<typeof STORE_POSTS, EntityMap<Post>>;

export const refreshPosts = (sort: Sort = 'new'): RefreshPosts => ({
  type: REFRESH_POSTS,
  payload: sort,
});

export const storePosts = (data: EntityMap<Post>): StorePosts => ({
  type: STORE_POSTS,
  payload: data,
});
