import {createStackNavigator} from 'react-navigation';
import {MAIN, DETAIL} from '../../navigation';
import Main from '../../main';
import Detail from '../../detail';

export default createStackNavigator(
  {
    [MAIN]: {
      screen: Main,
    },
    [DETAIL]: {
      screen: Detail,
    },
  },
  {initialRouteName: MAIN},
);
