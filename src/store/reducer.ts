import {combineReducers} from 'redux';
import {reducer as navigation} from '../navigation';
import {reducer as service} from '../service';
import {reducer as main} from '../main';

export default combineReducers({
  ...navigation,
  ...service,
  ...main,
});
