import {all, fork} from 'redux-saga/effects';
import {sagas as navigation} from '../navigation';
import {sagas as service} from '../service';
import {sagas as main} from '../main';

export default function* rootSaga() {
  const sagas = [...navigation, ...service, ...main].map(saga => fork(saga));
  yield all(sagas);
}
