import {put, takeEvery, takeLatest, select} from 'redux-saga/effects';
import {navigate, DETAIL} from '../../navigation';
import {CHOOSE_POST, ChooseSort, CHOOSE_SORT} from './actions';
import {sort} from './selectors';
import {Sort, refreshPosts} from '../../service';

export function* onChoosePost() {
  yield put(navigate({routeName: DETAIL}));
}
export function* onChooseSort({payload}: ChooseSort) {
  yield put(refreshPosts(payload));
}

export function* listen() {
  yield takeEvery(CHOOSE_POST, onChoosePost);
  yield takeLatest(CHOOSE_SORT, onChooseSort);
}

export default [listen];
