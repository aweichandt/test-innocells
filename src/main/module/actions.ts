import {PayloadAction} from '../../redux-helpers';
import {Sort} from '../../service';

export const CHOOSE_POST = '@main/CHOOSE_POST';
export const CHOOSE_SORT = '@main/CHOOSE_SORT';

export type ChoosePost = PayloadAction<typeof CHOOSE_POST, string>;
export type ChooseSort = PayloadAction<typeof CHOOSE_SORT, Sort>;

export const choosePost = (id: string): ChoosePost => ({
  type: CHOOSE_POST,
  payload: id,
});
export const chooseSort = (sort: Sort): ChooseSort => ({
  type: CHOOSE_SORT,
  payload: sort,
});
