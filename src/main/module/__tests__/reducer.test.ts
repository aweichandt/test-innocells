import reducer, {initialState} from '../reducer';
import {choosePost, chooseSort} from '../actions';

describe('main reducer', () => {
  it('initialise with state', () => {
    expect(reducer(undefined, {type: '@@init'})).toEqual(initialState);
  });
  it('choose post', () => {
    expect(reducer(initialState, choosePost('id'))).toEqual({
      ...initialState,
      chosenPostId: 'id',
    });
  });
  it('set sort', () => {
    expect(reducer(initialState, chooseSort('top'))).toEqual({
      ...initialState,
      sort: 'top',
    });
  });
});
