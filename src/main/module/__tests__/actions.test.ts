import {choosePost, chooseSort} from '../actions';

describe('main actions', () => {
  it('choose posts', () => {
    expect(choosePost('id')).toMatchSnapshot();
  });
  it('choose sort', () => {
    expect(chooseSort('top')).toMatchSnapshot();
  });
});
