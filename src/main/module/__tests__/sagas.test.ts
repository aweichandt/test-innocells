import {put, takeEvery, takeLatest} from 'redux-saga/effects';
import {navigate, DETAIL} from '../../../navigation';
import {CHOOSE_POST, CHOOSE_SORT, chooseSort} from '.././actions';
import {onChoosePost, listen, onChooseSort} from '../sagas';
import {refreshPosts} from '../../../service';

describe('main sagas', () => {
  it('on choose post', () => {
    const gen = onChoosePost();
    expect(gen.next().value).toEqual(put(navigate({routeName: DETAIL})));
    expect(gen.next().done).toBeTruthy();
  });
  it('on choose sort', () => {
    const action = chooseSort('top');
    const gen = onChooseSort(action);
    expect(gen.next().value).toEqual(put(refreshPosts('top')));
    expect(gen.next().done).toBeTruthy();
  });
  it('listen', () => {
    const gen = listen();
    expect(gen.next().value).toEqual(takeEvery(CHOOSE_POST, onChoosePost));
    expect(gen.next().value).toEqual(takeLatest(CHOOSE_SORT, onChooseSort));
    expect(gen.next().done).toBeTruthy();
  });
});
