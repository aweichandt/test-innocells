import {chosenPostId, sort} from '../selectors';
import {StateWithMain} from '../types';

const state: StateWithMain = {
  main: {
    chosenPostId: 'id',
    sort: 'new',
  },
};

describe('main selectors', () => {
  it('get posts', () => {
    expect(chosenPostId(state)).toEqual('id');
  });
  it('get sort', () => {
    expect(sort(state)).toEqual('new');
  });
});
