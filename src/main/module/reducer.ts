import {State} from './types';
import {ChoosePost, CHOOSE_POST, ChooseSort, CHOOSE_SORT} from './actions';
import {createReducer} from '../../redux-helpers';

export const initialState: State = {
  chosenPostId: '',
  sort: 'new',
};

const onChoosePost = (state: State, {payload}: ChoosePost): State => ({
  ...state,
  chosenPostId: payload,
});

const onChooseSort = (state: State, {payload}: ChooseSort): State => ({
  ...state,
  sort: payload,
});

const handlers = Object.freeze({
  [CHOOSE_POST]: onChoosePost,
  [CHOOSE_SORT]: onChooseSort,
});

export default createReducer(handlers, initialState);
