import {StateWithMain, State} from './types';
import {Selector, createSelector} from 'reselect';
import {Sort} from '../../service';

const stateSelector = ({main}: StateWithMain): State => main;

export const chosenPostId: Selector<StateWithMain, string> = createSelector(
  stateSelector,
  ({chosenPostId}) => chosenPostId,
);
export const sort: Selector<StateWithMain, Sort> = createSelector(
  stateSelector,
  ({sort}) => sort,
);
