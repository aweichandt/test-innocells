import {Sort} from '../../service';

export type State = {
  chosenPostId: string;
  sort: Sort;
};

export type StateWithMain = {
  main: State;
};
