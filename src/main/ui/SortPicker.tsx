import React, {useCallback, Fragment} from 'react';
import {Picker} from 'react-native';
import styled from 'styled-components/native';
import {Sort} from '../../service/module/types';

type Props = {
  sort: Sort;
  setSort: (v: Sort) => void;
};

const SortOptions: Sort[] = ['new', 'top', 'hot', 'controversial'];

const Container = styled.View`
  background-color: lightgray;
`;
const Title = styled.Text`
  text-align: center;
`;

const SortPicker: React.FC<Props> = ({sort, setSort}) => {
  const onValueChange = useCallback(
    value => {
      if (sort !== value) {
        setSort(value);
      }
    },
    [setSort, sort],
  );
  return (
    <Container>
      <Title>Sort Order</Title>
      <Picker selectedValue={sort} onValueChange={onValueChange}>
        {SortOptions.map(v => (
          <Picker.Item key={v} label={v} value={v} />
        ))}
      </Picker>
    </Container>
  );
};

export default SortPicker;
