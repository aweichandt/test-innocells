import React, {useState} from 'react';
import styled from 'styled-components/native';
import {FlatList} from 'react-native';
import {useCallback} from 'react';
import {Sort} from '../../service';

type Props = {
  items: string[];
  Item: React.ComponentType<{id: string}>;
  Header: React.ComponentType<{}> | undefined;
  refresh: (sort: Sort) => void;
  refreshing: boolean;
  sort: Sort;
};

const EmptyText = styled.Text`
  font-size: 20;
  flex: 1;
  align-self: center;
  justify-content: center;
  text-align: center;
`;
const EmptyComponent: React.FC = () => (
  <EmptyText>Pull to load items</EmptyText>
);

const List: React.FC<Props> = ({
  items,
  Item,
  Header,
  refresh,
  refreshing,
  sort,
}) => {
  const renderItem = useCallback(({item: id}) => <Item id={id} />, [Item]);
  const keyExtractor = useCallback(id => id, []);
  const onRefresh = useCallback(() => refresh(sort), [refresh, sort]);
  return (
    <FlatList
      data={items}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      onRefresh={onRefresh}
      refreshing={refreshing}
      ListEmptyComponent={EmptyComponent}
      ListHeaderComponent={Header}
    />
  );
};

export default List;
