import React from 'react';
import styled from 'styled-components/native';
import moment from 'moment';
import {Post} from '../../service/module';
import {useMemo} from 'react';

const Container = styled.TouchableOpacity`
  border: 1px solid lightgray;
`;
const Row = styled.View`
  flex-direction: row;
  padding: 5px 0;
`;
const Column = styled.View<{width: string}>`
  width: ${props => props.width || '100%'};
  flex-direction: column;
  padding: 0 5px;
`;
const Title = styled.Text`
  width: 100%;
`;
const Date = styled.Text`
  text-align: right;
  width: 100%;
  font-size: 12;
`;
const Author = styled.Text`
  width: 50%;
  font-size: 12;
  font-weight: bold;
`;
const Score = styled.Text`
  width: 25%;
  font-size: 12;
`;
const Comments = styled.Text`
  width: 25%;
  font-size: 12;
`;
const Thumbnail = styled.Image`
  flex: 1;
  resize-mode: contain;
`;

type Props = Partial<Post> & {onPress: () => void};

const PostView: React.FC<Props> = ({
  title,
  author,
  score,
  num_comments,
  created_utc,
  thumbnail,
  thumbnail_width,
  thumbnail_height,
  onPress,
}) => {
  const strDate = useMemo(() => moment.unix(created_utc).fromNow(), [
    created_utc,
  ]);
  return (
    <Container onPress={onPress}>
      <Row>
        <Column width="25%">
          <Thumbnail
            source={{uri: thumbnail}}
            width={thumbnail_width}
            height={thumbnail_height}
          />
        </Column>
        <Column width="75%">
          <Row>
            <Date>{strDate}</Date>
          </Row>
          <Row>
            <Title>{title}</Title>
          </Row>
          <Row>
            <Author>By: {author}</Author>
            <Score>Score: {score}</Score>
            <Comments>Comments:{num_comments}</Comments>
          </Row>
        </Column>
      </Row>
    </Container>
  );
};

export default PostView;
