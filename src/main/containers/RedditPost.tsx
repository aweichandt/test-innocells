import {MapStateToProps, MapDispatchToProps, connect} from 'react-redux';
import {Post, StateWithService, postWithId} from '../../service/module';
import {navigate, DETAIL} from '../../navigation';
import {choosePost} from '../module';
import {Post as PostView} from '../ui';

type Props = {id: string};
type SProps = Partial<Post>;
type DProps = {onPress: () => void};

const mapStateToProps: MapStateToProps<SProps, Props, StateWithService> = (
  state,
  props,
) => postWithId(state, props) || {};
const mapDispatchToProps: MapDispatchToProps<DProps, Props> = (
  dispatch,
  {id},
) => ({
  onPress: () => dispatch(choosePost(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PostView);
