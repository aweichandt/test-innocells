import {Sort} from '../../service';
import {MapStateToProps, MapDispatchToProps, connect} from 'react-redux';
import {StateWithMain, sort, chooseSort} from '../module';
import {SortPicker} from '../ui';

type Props = {};
type SProps = {sort: Sort};
type DProps = {setSort: (v: Sort) => void};

const mapStateToProps: MapStateToProps<
  SProps,
  Props,
  StateWithMain
> = state => ({
  sort: sort(state),
});

const mapDispatchToProps: MapDispatchToProps<DProps, Props> = dispatch => ({
  setSort: (sort: Sort) => dispatch(chooseSort(sort)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SortPicker);
