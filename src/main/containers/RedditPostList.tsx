import React from 'react';
import {MapStateToProps, connect, MapDispatchToProps} from 'react-redux';
import {
  StateWithService,
  postIds,
  refreshing,
  refreshPosts,
  Sort,
} from '../../service';
import {List} from '../ui';
import RedditPost from './RedditPost';
import RedditSortPicker from './RedditSortPicker';
import {sort, StateWithMain} from '../module';

type Props = {
  Item: React.ComponentType<{id: string}>;
  Header: React.ComponentType<{}>;
};
type SProps = {
  items: string[];
  refreshing: boolean;
  sort: Sort;
};
type DProps = {
  refresh: (sort: Sort) => void;
};
const mapStateToProps: MapStateToProps<
  SProps,
  Props,
  StateWithService & StateWithMain
> = state => ({
  items: postIds(state),
  refreshing: refreshing(state),
  sort: sort(state),
});

const mapDispatchToProps: MapDispatchToProps<DProps, Props> = dispatch => ({
  refresh: (sort: Sort) => dispatch(refreshPosts(sort)),
});

const PostsList = connect(
  mapStateToProps,
  mapDispatchToProps,
)(List);

const RedditPostList: React.FC = () => (
  <PostsList Item={RedditPost} Header={RedditSortPicker} />
);

export default RedditPostList;
