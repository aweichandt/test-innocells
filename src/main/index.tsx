import React from 'react';
import {SafeAreaView} from 'react-native';
import {RedditPostList} from './containers';

export * from './module';

const Screen: React.FC = () => {
  return (
    <SafeAreaView>
      <RedditPostList />
    </SafeAreaView>
  );
};

export default Screen;
